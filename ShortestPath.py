import pygraph.graph
import math
import heapq
import numpy as np
#==========================================UTILITY FUNCTIONS==================================================#
def InitializeSingleSource(graph, source):
    '''
    Sets each vertex to have a estiamted distance of infinity and prdecessor to NILL
    @Params, graph - graph to be initialized, source - source vertex
    @Returns - graph with source vertex updated to have v.d = 0
    '''
    for v in graph.vertices:
        vertex = graph.vertices[v]
        vertex['spEstimate'] = math.inf
        vertex['pred'] = None

    if source in graph:
        source = graph.vertices[source]
        source['spEstimate'] = 0
    return graph

def Relax(vertexV, vertexU, edgeUV, graph):
    '''
    Relaxes v.d shortest path estimate
    @Params: vertexV - vertex being relaxed
             vertexU - vertex which we are checking if a new shortest path can be found from
             edgeUV  - edge which weight we are checking agaisnt shortest path estimate for vertexV
             graph   - input graph that is being relaxed
    @Returns: vertexV[spEstimate] - the new estimate for the vertex V shortestPath.
              vertexV[pred] - new predecessor vertex for vertex V
    '''
    #getting the vertex view from the vertex to get estiamtes and weights
    vertexViewV = graph.vertices[vertexV]
    vertexViewU = graph.vertices[vertexU]

    if vertexViewV['spEstimate'] > vertexViewU['spEstimate'] + edgeUV["weight"]:
        vertexViewV['spEstimate'] = vertexViewU['spEstimate'] + edgeUV["weight"]
        #assings vertex number not the vertexView class
        vertexViewV['pred'] = vertexU
    return vertexViewV['spEstimate'], vertexViewV['pred']

def MinPriorityQueue(graph):
    '''
    From a graph creates a minMaxPriorityQueue assuming graph has been initialized first
    Params:  graph   - input graph whos vertices are being made into priority queue
    Returns: minPrioirtyQueue - priority queue
    '''
    minPrioirtyQueue = []

    #add each vertex to heap as a tuple of weight and vertex
    for vertex in graph.vertices:
        vertexV = graph.vertices[vertex]
        minPrioirtyQueue.append([vertexV['spEstimate'],vertex])

    return minPrioirtyQueue

def CreateWeightedMatrix(graph):
    '''
    Creates a weighted matrix from a graph
    Params: graph - graph which the weighted matrix is being made from
    Returns: weightMatrix - weighted matrix for the input graph
    '''
    ##converts adjaceny_matrix to np.array
    weightMatrix = np.array(graph.adjacency_matrix(attr_name='weight'))
    weightMatrix = weightMatrix.astype(float)
    #makes non-existent edges infinity
    for row in range(0, len(weightMatrix)):
        for cell in range(0, len(weightMatrix[0])):
            if row != cell and weightMatrix[row][cell] == 0:
                weightMatrix[row][cell] = math.inf
    return weightMatrix

    
#======================================== GRAPHS =======================================================#
def CreateWeightedDirectedGraph():
    '''
    Creates a test weighted directed graph that will be used for testing.
    @Params:
    @Returns: graph - weighted directed graph G
    '''
    graph = pygraph.graph.Graph(tag="Directed-Weighted")
    for i in range(0,5):
        graph.add_vertex(i, pred=None, spEstimate=None)

    graph.add_edge(0,1, weight=3)
    graph.add_edge(0,3, weight=5)
    graph.add_edge(1,2, weight=6)
    graph.add_edge(1,3, weight=2)
    graph.add_edge(2,4, weight=2)
    graph.add_edge(3,1, weight=1)
    graph.add_edge(3,2, weight=4)
    graph.add_edge(3,4, weight=6)
    graph.add_edge(4,2, weight=7)
    graph.add_edge(4,0, weight=3)

    return graph

def CreateNegativeWeightCycleGraph():
    '''
    Creates a test weighted directed graph that contains a negative weight cycle,
    that will be used for testing.
    @Params:
    @Returns: graph - weighted directed graph G that has a negative weight cycle.
    '''
    graph = pygraph.graph.Graph(tag="NegativeWeightCycle")
    for i in range(0,6):
        graph.add_vertex(i, pred=None, spEstimate=None)

    graph.add_edge(0,1,weight=2)
    graph.add_edge(1,2,weight=2)
    graph.add_edge(2,3,weight=2)
    graph.add_edge(2,4,weight=-5)
    graph.add_edge(4,5,weight=-5)
    graph.add_edge(5,0,weight=-5)

    return graph

def CreateWeightedDirectedNegativeGraph():
    '''
    Creates a weighted directed graph with some negative edge weights
    @Params:
    @Returns: graph - weighted directed graph with negative edges
    '''
    graph = pygraph.graph.Graph(tag="Directed-Weighted")
    for i in range(0,5):
        graph.add_vertex(i, pred=None, spEstimate=None)

    graph.add_edge(0,1, weight=6)
    graph.add_edge(0,3, weight=7)
    graph.add_edge(1,2, weight=5)
    graph.add_edge(1,3, weight=8)
    graph.add_edge(1,4, weight=-4)
    graph.add_edge(2,1, weight=-2)
    graph.add_edge(3,2, weight=-3)
    graph.add_edge(3,4, weight=9)
    graph.add_edge(4,2, weight=7)
    graph.add_edge(4,0, weight=2)

    return graph

def CreateWeightedDirectedNegativeGraphTwo():
    graph = pygraph.graph.Graph(tag="Directed-Weighted")
    for i in range(0,4):
        graph.add_vertex(i, pred=None, spEstimate=None)
    graph.add_edge(0,1, weight=-5)
    graph.add_edge(0,2, weight=2)
    graph.add_edge(0,3, weight=3)
    graph.add_edge(2,3, weight=1)
    graph.add_edge(1,2, weight=4)

    return graph


def CreateWeightedDirectedGraphTwo():
    graph = pygraph.graph.Graph(tag="Directed-Weighted")
    for i in range(0,4):
        graph.add_vertex(i, pred=None, spEstimate=None)
    graph.add_edge(0,1, weight=10)
    graph.add_edge(1,3, weight=1)
    graph.add_edge(2,1, weight=3)
    graph.add_edge(1,2, weight=-2)
    graph.add_edge(0,2, weight=5)
    graph.add_edge(3,2, weight=-3)
    graph.add_edge(2,3, weight=9)
    
    return graph

def CreateWeightedDirectedGraphThree():
    graph = pygraph.graph.Graph(tag="Directed-Weighted")
    for i in range(0,4):
        graph.add_vertex(i, pred=None, spEstimate=None)
    graph.add_edge(0,1, weight=10)
    graph.add_edge(1,3, weight=1)
    graph.add_edge(2,1, weight=0)
    graph.add_edge(1,2, weight=1)
    graph.add_edge(0,2, weight=8)
    graph.add_edge(3,2, weight=0)
    graph.add_edge(2,3, weight=6)
    
    return graph

#============================================Algorithm Classes===================================================#
class SingleSourceAlgorithm():
    def __init__(self, graph):
        self.graph = graph

    def get_shortest_path(self,source, vertex, pathVertices):
        '''
        recursive function that returns shotest path from source to destination
        Params : graph - graph in which shortest path is being printed
                 source - source vertex of shortest path
                vertex - current vertex that is along shortest path
                 pathVertices - array containing vertices along shortest path
        Returns: pathVertices - array containing list of each one of the vertices from source to destination
     '''
        if vertex == source:
            pathVertices.append(vertex)
            #order reversed so path prints out from source -> destination
            pathVertices.reverse()
            
            return pathVertices

        else:
            #if vertex isn't source it add the vertex to list and recalls the function of predecessorVertex
            pathVertices.append(vertex)

            #checks case where a source vertex is choosen where there are no edges to any other vertices
            try:
                predecessorVertex = self.graph.vertices[vertex]['pred']
            except:
                print("No Path from Source -> Destination")
                return   
            self.get_shortest_path(source,predecessorVertex, pathVertices)
        return pathVertices

    def get_all_shortest_paths(self, source):
        '''
        Returns list of shortest paths from single source to all other vertex in the graph
        Params: Source - source vertex that algorithm has been ran using
        Returns: listOfPaths - List of all paths from single source to all other vertices
        '''
        listOfPaths = []

        for vertex in self.graph.vertices:
            listOfPaths.append(self.get_shortest_path(source, vertex, []))

        return listOfPaths                                

    def get_path_weight(self, destination):
        '''
        function that returns path weight
        Params: destination - destination vertex for shortest path weight
        Returns: totalWeightOfPath - total path weight from source vertex to destination
        '''

        if self.graph.vertices[destination]['spEstimate'] == math.inf:
            return math.inf

        totalWeightOfPath = 0
    
        currentVertex = self.graph.vertices[destination]
        currentVertexValue = destination
        while(currentVertex['pred'] != None):
            totalWeightOfPath = totalWeightOfPath + self.graph.edges[currentVertex['pred']][currentVertexValue]['weight']
            currentVertexValue = currentVertex['pred']
            currentVertex = self.graph.vertices[currentVertexValue]
        return totalWeightOfPath

    def get_all_path_weights(self):
        '''
        Returns list of path weight for each vertex from the source
        Params:
        Returns: listOfPathWeights - list of all path weights
        '''
        listOfPathWeights = []
        for vertex in self.graph.vertices:
            listOfPathWeights.append(self.get_path_weight(vertex))
        return listOfPathWeights

    def print_shortest_path(self,source, vertex, pathVertices):
        '''
        recursive function that prints shotest path from source to destination
        Params :
                source - source vertex of shortest path
                vertex - current vertex that is along shortest path
                pathVertices - array containing vertices along shortest path
        Returns: 
     '''
        if vertex == source:
            pathVertices.append(vertex)
            #order reversed so path prints out from source -> destination
            pathVertices.reverse()
            

        else:
            #if vertex isn't source it add the vertex to list and recalls the function of predecessorVertex
            pathVertices.append(vertex)
            #checks case where a source vertex is choosen where there are no edges to any other vertices
            try:
                predecessorVertex = self.graph.vertices[vertex]['pred']
            except:
                print("No Path from Source -> Destination")
                return None 
     
            self.get_shortest_path(source,predecessorVertex, pathVertices)
        print("Path from:",source,"->",pathVertices[-1])
        print(pathVertices)

    def print_all_shortest_paths(self,source):
        for vertex in self.graph.vertices:
           self.print_shortest_path(source, vertex, [])

    def print_path_weight(self,source,destination):
        '''
        function that prints path weight
        Params: source - source vertex of shortest path
                destination - destination vertex for shortest path weight
        Returns: 
        '''
        # source = -1
    
        if self.graph.vertices[destination]['spEstimate'] == math.inf:
            print("There exists no path between vertex", source, " -> ", destination)
            print("Therefore the weight of the path is: ", math.inf)
            return

        totalWeightOfPath = 0
    
        currentVertex = self.graph.vertices[destination]
        currentVertexValue = destination
        while(currentVertex['pred'] != None):
            totalWeightOfPath = totalWeightOfPath + self.graph.edges[currentVertex['pred']][currentVertexValue]['weight']
            currentVertexValue = currentVertex['pred']
            currentVertex = self.graph.vertices[currentVertexValue]
        
        print("Weight for:", source,"->",destination,"= ", totalWeightOfPath) 
        return

    def print_all_path_weights(self, source):
        '''
        function that prints all path weights between source and all other vertices in the graph
        Params: source - source vertex of shortest path
        Returns: 
        '''
        for vertex in self.graph.vertices:
            self.print_path_weight(source, vertex)                   

class BellmanFord(SingleSourceAlgorithm):
    def __init__(self, graph):
        self.graph = graph

    def run_algorithm(self, source):
        '''
    Bellamanford function finds shortest path between source vertex and all other verticies
    in a graph
    Params: graph - graph tha algorithm is running on
            source - source vertex the algorihtm is finding shortest paths from.
            destination - destination vertex to print out shortest path for user
    Returns: graph - graph with shortest path calculated
             boolean - True of False boolean signifying successful execution
    '''
        self.graph = InitializeSingleSource(self.graph, source)

        #loop over each edge |V|-1 times
        for count in range(0,self.graph.order()):

            #get vertices from edge to pass to relax function
            for edge in self.graph.edges:

                vertices = edge.get_incident_vertices()
                #getting repsective vertex from vertices array
                vertexV = self.graph.vertices[vertices[1]]
                vertexU = self.graph.vertices[vertices[0]]


                vertexV["spEstimate"], vertexV["pred"] = Relax(vertices[1], vertices[0], edge, self.graph)

        #checks for negative weight cycles
        for edge in self.graph.edges:
            vertices = edge.get_incident_vertices()
            vertexV = self.graph.vertices[vertices[1]]
            vertexU = self.graph.vertices[vertices[0]]
            if vertexV['spEstimate'] > vertexU['spEstimate'] + edge["weight"]:
                return self.graph, False

        return self.graph, True

class Dijkstra(SingleSourceAlgorithm):
    def __init__(self, graph):
        super().__init__(graph)

    def run_algorithm(self,source):
        '''
        Single source shortesst path aglorithm that finds a shortest path from one vertex to all others.
        Params: graph - graph tha algorithm is running on
                source - source vertex the algorihtm is finding shortest paths from.
                destination - destination vertex to print out shortest path for user
        Returns: graph - graph with shortest path calculated
                 boolean - True of False boolean signifying successful execution
        '''
        for edge in self.graph.edges:
            if edge['weight'] < 0:
                return self.graph, False
        #initialize graph vertices
        self.graph = InitializeSingleSource(self.graph,source)   
        #during execution invariant minPrioirtyQueue = vertices - poppedVertices remains true
        poppedVertices = []
        verticesQueue = MinPriorityQueue(self.graph)
    
        while len(verticesQueue) != 0:
         #gets smallest value off queue and removes it
            minVertex = min(verticesQueue, key=lambda x:x[0])
            queuePosition = verticesQueue.index(minVertex)
            verticesQueue.pop(queuePosition)
            poppedVertices.append(minVertex[1])

            neighbourVertices = self.graph.neighbours(minVertex[1])
            #relaxes each edge of neighbour vertices of popped vertex
            for vertex in neighbourVertices:
                vertexV = self.graph.vertices[vertex]
               #before we relax we need the index of the vertexV in the queue
                tempVertex = [vertexV["spEstimate"], vertex]
                #If the index is not in the queue then vertexV is not in the queue thuse we do not need to relax
                try:
                    index = verticesQueue.index(tempVertex)
                except:
                    continue
                
                vertexV["spEstimate"], vertexV["pred"] = Relax(vertex, minVertex[1],self.graph.edges[minVertex[1]][vertex], self.graph)
                #Here we update the queue priority for the relaxed edge      
                verticesQueue[index][0] = vertexV["spEstimate"]
        
        
        return self.graph, True

class AllPairsAlgorithm():
    def __init__(self, graph):
        self.graph = graph

    def get_shortest_path(self, predecessorMatrix, source, destination):
        '''
        Function follows the predecessor matrix by getting the value in matrix[source][destination]
        and then making destination equal to that value to get the next vertex along the shortest path
        Params: predecessorMatrix - matrix contaning predcessor vertex for all pairs shortest path algorithm
                source - source vertex we want to print
                destination - destination vertex we want to find shortest path to
        Returns: verticesList - list of vertices along path from source -> destination        
        '''
        verticesList = []
        verticesList.append(destination)

        currentVertex = destination
        while(currentVertex != source):

            #need to convert predecessorMatrix values to int from float to prevent indice error when accessing later valeus
            currentVertex = int(currentVertex)
            try:
                verticesList.append(int(predecessorMatrix[source][currentVertex]))
            except:
                print("No Shortest Path exists between ", source , "-> ", destination)   
                return 
            currentVertex = predecessorMatrix[source][currentVertex]
        verticesList.reverse()

        return verticesList

    def get_all_pair_shortest_paths(self, predecessorMatrix):
        '''
        Returns list of all shortest paths from source to destination
        Params: predecessorMatrix - matrix containing predecessor for each vertex
        Returns: allShortestPathsList - 2d array containing all pairs of shortest paths
        '''
        allShortestPathList = []

        #returns 2d array of all shortest path vertices
        for vertex in self.graph.vertices:
            for _vertex in self.graph.vertices:
                allShortestPathList.append(self.get_shortest_path(predecessorMatrix,vertex,_vertex))
        return allShortestPathList

    def get_path_weight(self, weightMatrix, source, destination):
        '''
        Returns weight of path using weightMatrix
        Params: weightMatrix - weights of shortest paths stored in matrix
                source - source vertex
                destination - destination vertex
        Returns: weightMatrix[source][destination] - path weight        
        '''
        return weightMatrix[source][destination]

    def get_all_path_weights(self, weightMatrix):
        '''
        Returns weight of all paths using weightMatrix
        Params: weightMatrix - weights of shortest paths stored in matrix
                source - source vertex
                destination - destination vertex
        Returns: allweightList - list of all path weights        
        '''
        allWeightsList = []
        for vertex in self.graph.vertices:
            for _vertex in self.graph.vertices:
                allWeightsList.append(self.get_path_weight(weightMatrix, vertex, _vertex))
        return allWeightsList

    def print_shortest_path(self, predecessorMatrix,source,destination):
        '''
        Function follows the predecessor matrix by getting the value in matrix[source][destination]
        and then making destination equal to that value to get the next vertex along the shortest path to print path
        Params: predecessorMatrix - matrix contaning predcessor vertex for all pairs shortest path algorithm
                source - source vertex we want to print
                destination - destination vertex we want to find shortest path to
        Returns: 
        '''
        verticesList = []
        verticesList.append(destination)

        currentVertex = destination
        while(currentVertex != source):

            #need to convert predecessorMatrix values to int from float to prevent indice error when accessing later valeus
            currentVertex = int(currentVertex)
            try:
                verticesList.append(int(predecessorMatrix[source][currentVertex]))
            except:
                print("No Shortest Path exists between ", source , "-> ", destination)   
                return
            currentVertex = predecessorMatrix[source][currentVertex]
        verticesList.reverse()
        print("Path from:", source, "->", destination)
        print(verticesList)

    def print_all_shortest_paths(self,predecessorMatrix):
        for vertex in self.graph.vertices:
            for _vertex in self.graph.vertices:
                self.print_shortest_path(predecessorMatrix, vertex, _vertex)

        
    def print_path_weight(self, weightMatrix,source, destination):
        '''
        prints weight of path using weightMatrix
        Params: weightMatrix - weights of shortest paths stored in matrix
                source - source vertex
                destination - destination vertex
        Returns: weightMatrix[source][destination] - path weight        
        '''
        print("Weight for:", source,"->",destination,"= ", weightMatrix[source][destination]) 

    def print_all_path_weights(self,weightMatrix):
        for vertex in self.graph.vertices:
            for _vertex in self.graph.vertices:
                self.print_path_weight(weightMatrix,vertex,_vertex)    

class FloydWarshall(AllPairsAlgorithm):
    def __init__(self, graph):
        super().__init__(graph)

    def run_algorithm(self):
        '''
        All Pairs shortest path algorithm that finds shortest path between all vertices
        Params: graph - graph tha algorithm is running on
                source - source vertex the algorihtm is finding shortest paths from.
                destination - destination vertex to print out shortest path for user
        Returns: graph - graph with shortest path calculated         
        '''
        success = False
        weightMatrix = CreateWeightedMatrix(self.graph)
        lengthOfRow = len(weightMatrix)
        predecessorMatrix = np.full((lengthOfRow, lengthOfRow), math.inf)
        #loop iterates for when k = 0 to create predecessor matrix
        for vertexI in range(0, lengthOfRow):
            for vertexJ in range(0, lengthOfRow):

                if vertexI != vertexJ and weightMatrix[vertexI][vertexJ] != math.inf:
                    predecessorMatrix[vertexI][vertexJ] = vertexI

                # weightMatrix[vertexI][vertexJ] = min(weightMatrix[vertexI][vertexJ], weightMatrix[vertexI][0] + weightMatrix[0][vertexJ])
                if weightMatrix[vertexI][vertexJ] > weightMatrix[vertexI][0] + weightMatrix[0][vertexJ]:
                    weightMatrix[vertexI][vertexJ] = weightMatrix[vertexI][0] + weightMatrix[0][vertexJ]
                    predecessorMatrix[vertexI][vertexJ] = predecessorMatrix[0][vertexJ]
      
        #loops through weighted graph and update the weights, converging on path of minimum weight
        for vertexK in range(1,lengthOfRow):
            for vertexI in range(0, lengthOfRow):
                for vertexJ in range(0, lengthOfRow):
                    #calculates shorterest path route from ij by checking ik->jk
                    if weightMatrix[vertexI][vertexJ] > weightMatrix[vertexI][vertexK] + weightMatrix[vertexK][vertexJ]:
                        weightMatrix[vertexI][vertexJ] = weightMatrix[vertexI][vertexK] + weightMatrix[vertexK][vertexJ]
                        predecessorMatrix[vertexI][vertexJ] = predecessorMatrix[vertexK][vertexJ]

        #checks for negative weight cycles
        for vertex in self.graph.vertices:
            if weightMatrix[vertex,vertex] < 0:
                return weightMatrix, predecessorMatrix, success

        success = True
        #check for neative weight cycle does not fail so return true
        return weightMatrix, predecessorMatrix, success

class Johnsons(AllPairsAlgorithm): 
    def __init__(self, graph):
        super().__init__(graph)

    def run_algorithm(self):
        '''
        All pairs shortest path algorithm that finds shortest path between all verticies
        Bellmanford is used to reweigh the edge weights and dijkstras is used for each edge
        Params: graph - graph tha algorithm is running on
                source - source vertex the algorihtm is finding shortest paths from.
                destination - destination vertex to print out shortest path for user
        Returns:weightMatrix - a weighted matrix containing weight of path in matrix[source][destination]
                predecessorMatrix - matrix of predecessor vertices 
        '''
        #Add the extra vertex, use bellman ford, new graph with new weights, then run dijkstra on each vertex.
        newSourceVertex = len(self.graph.vertices)
        self.graph.add_vertex(newSourceVertex, pred=None, spEstimate=None)

        #new edges added
        for graphVertex in self.graph.vertices:
            self.graph.add_edge(newSourceVertex, graphVertex, weight = 0)
    
        # run Bellman-ford, success checks for negative weight cycle
        success = True

        bellmanFord = BellmanFord(self.graph)
        self.graph, success = bellmanFord.run_algorithm(newSourceVertex)

        #negative weight cycle detected
        if(success == False):
            return self.graph, False

        #now remove the added vertex and update edge weights so newWeight = w(u,v) + h[u] - h[v] where H is the shortest path estimate
        self.graph.remove_vertex(newSourceVertex)
        for edge in self.graph.edges:
            vertices = edge.get_incident_vertices()
            vertexU = self.graph.vertices[vertices[0]]
            vertexV = self.graph.vertices[vertices[1]]
            originalWeight = edge['weight']
            edge['weight'] = originalWeight + vertexU['spEstimate'] - vertexV['spEstimate']

        #store bellman Estimates to use for final weight
        bellmanSPEstimates = []

        for vertex in self.graph.vertices:
            bellmanSPEstimates.append(self.graph.vertices[vertex]['spEstimate'])

        print("Running Dijkstra...")
        #runs on a copy of the new graph
        graphCopy = self.graph

        weightMatrix = CreateWeightedMatrix(self.graph)
        predecessorMatrix = np.full(weightMatrix.shape, math.inf)
    
        #Run Dijkstra on vertex u as source, then add all delta(u,v) to weightMatrix[u][v] for all v
        for vertex in self.graph.vertices:

            dijkstra = Dijkstra(graphCopy)
            graphCopy, success = dijkstra.run_algorithm(vertex)

            for _vertex in self.graph.vertices:
                weightMatrix[vertex][_vertex] = dijkstra.get_path_weight(_vertex) + bellmanSPEstimates[_vertex] - bellmanSPEstimates[vertex]
                if vertex != _vertex:

                    #reset graphs may have None set as the pred so we shall make this math.inf
                    if graphCopy.vertices[_vertex]['pred'] == None:
                        predecessorMatrix[vertex][_vertex] = math.inf
                    else:
                        predecessorMatrix[vertex][_vertex] = graphCopy.vertices[_vertex]['pred']
            #reset graph to intial Graph
            graphCopy = self.graph



    
        return weightMatrix, predecessorMatrix, success    



