import pygraph.graph
import ShortestPath as SP
import generateGraph as GP
import pickle
import os
from os import path
import sys
import time
from openpyxl import Workbook
import pandas as pd
from openpyxl import load_workbook
'''
The testing harness has the job to find import the binary file for a respective graph,
find the vertex which is fully connected to the rest of the graph.
Then run the algorithms on the graph 3 times each, outputting the times and their average to an excel file.   
'''
ARGVERTICES = -1
ARGEDGES = -1

def argumentHandeler():
    '''
        Function checks that the args passed to script are valid integers
        Params: 
        Returns: vertices - integer for vertices passed as arg1, edges - integer of edges passed as arg2, sheetname - name of sheet to write result to 
    '''
    #check argument passed for vertices is a valid Integer
    try:
        vertices = int(sys.argv[1])
    except:
        print("Argument passed for vertices was not an integer\n")
        print("Pass a valid integer")
        exit(1)

    try:
        edges = int(sys.argv[2])
    except:
        print("Argument passed for edges was not an integer\n")
        print("Pass a valid integer")
        exit(1)

    if vertices < 0 or edges < 0:
        print("Please input a positve integer for the vertices or edges")
        exit(1)

    try:
        sheetname = sys.argv[3]
    except:
        print("Please enter a valid Sheetname to write result to")            
    return vertices, edges, sheetname

def unpickleGraphObject(vertices, edges):
    '''
        This function takes in the number of vertices and edges desired, gets the unpickled graph,
        unpickles the graph and returns the graph object.
    Params: vertices - number of vertices
            edges - number of edges
    Returns: Graph - graph object to be tested.
    '''
    graphFileName = "Graph-" + str(vertices) + "-"+str(edges)+".P"
    print("Finding ", graphFileName, " ...")

    #check if file exists in Graphs folder, if not then throw error
    graphPath = "Graphs/" + graphFileName
    if not path.exists(graphPath):
        print("The Graph could not be found.")
        print("Please check the vertices and Edges input.")

    #now we open file
    graphBinaryFile = open(graphPath, 'rb')

    #then unpickle graph object
    graph = pickle.load(graphBinaryFile)

    return graph  

def findConnectedVertex(graph):
    '''
        We use this to find the connected vertex in the graph by doing BFS on each vertex till the count returned
        equals the number of vertices in the graph e.g. count == ARGVERTICES. This is done once per graph
    Params: graph - the graph object we searching
    Return: source - the source vertex for SSSP algorithms
    '''
    for vertex in graph.vertices:
        count = GP.breadthFirstSearch(vertex,graph)
        
        #returns the source vertex that is fully connected to all other vertices
        if count == ARGVERTICES:
            return vertex

        count = 0    

def testAlgorithm(algorithm, source):
    '''
        runs algorithm and times how long each execution takes using time library
    Params: algorithm - the class of the algorithm being ran
    Returns: elapsedTime - elapsed time for algorithm to execute on the graph
    '''
    #check if Bellman or Dijkstra
    if isinstance(algorithm, SP.SingleSourceAlgorithm):
        print("Beginning Test...")
        start = time.clock()
        graph, success = algorithm.run_algorithm(source)
        end = time.clock()
        print("Start Time: ",start)
        print("End Time: ",end)
        elsapsedTime = end - start
        #check success is true
        if success:
            return elsapsedTime

        #False so set NAN
        elsapsedTime = None    
        return elsapsedTime

    #this is for APSP algorithms
    print("Beginning Test...")
    start = time.clock()
    weightMatrix, predecessorMatrix, success = algorithm.run_algorithm()
    end = time.clock()
    print("Start Time: ",start)
    print("End Time: ",end)
    elsapsedTime = end - start

    #check success is true
    if success:
        return elsapsedTime

    elsapsedTime = None    
    return elsapsedTime   

def conductExperiment(algorithms, source):
    '''
        Tests each of the algorithms on the specified graph 5 times
        returns the times as a 2d Array.
    Params: algorithms - Array of algorithms to be tested
            source - source vertex for tested graph for SSSP algorithms
    Returns: results - 2D array of each time recorded    
    '''
    results = []

    for algorithm in algorithms:
        #repeats test five times
        algResults = []
        for count in range(0,5):
            time = testAlgorithm(algorithm, source)
            algResults.append(time)

        results.append(algResults)

    return results        

def exportResults(results, filename, sheetname):
    '''
        Function takes in the results for each of the algorithms, orders them into test number and then
        uses a panda frame to append the results to the pandas worksheet
    '''
    test1 = []
    test2 = []
    test3 = []
    test4 = []
    test5 = []

    algorithmNames = ['Bellman-Ford','Dijkstra','Floyd-Warshall','Johnson\'s']
    #we have added the result into individual columns 
    for algorithmsResults in results:
        test1.append(algorithmsResults[0])
        test2.append(algorithmsResults[1])
        test3.append(algorithmsResults[2])
        test4.append(algorithmsResults[3])
        test5.append(algorithmsResults[4])

    #creates a dictionary of the results
    resultDict = {'Algorithms':algorithmNames, 'Test 1':test1, 'Test 2':test2, 'Test 3':test3, 'Test 4':test4, 'Test 5':test5}
    writer = pd.ExcelWriter(filename, engine="openpyxl", mode='a')
    writer.book = load_workbook(filename)

    #sheetname default = 'Sheet1'
    print(writer.book[sheetname].max_row)
    startRow = writer.book[sheetname].max_row
    #copy exsisting sheet
    writer.sheets = dict((ws.title, ws)for ws in writer.book.worksheets)
    #create a new pandas dataframe
    dataFrame = pd.DataFrame(resultDict)        

    dataFrame.to_excel(writer,index=False,header=True,startrow=startRow + 2, sheet_name=sheetname)
    writer.save()
    writer.close()
###############################MAIN#########################
ARGVERTICES, ARGEDGES, ARGSHEETNAME = argumentHandeler()

graph = unpickleGraphObject(ARGVERTICES, ARGEDGES)

sourceVertexForSSSP = findConnectedVertex(graph) 

#creates graph objects for each algorithm we shall run on the graph
bellman = SP.BellmanFord(graph)
dijkstra = SP.Dijkstra(graph)
floyd = SP.FloydWarshall(graph)
johnsons = SP.Johnsons(graph)

algorithmObjects = [bellman,dijkstra,floyd,johnsons]

results = conductExperiment(algorithmObjects, sourceVertexForSSSP)

exportResults(results,'ExperimentResults.xlsx',ARGSHEETNAME)
print(results)