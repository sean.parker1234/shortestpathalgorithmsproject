import pygraph.graph
import math
import sys
import random
import queue
import pickle

##############GLOBAL VARIABLES#######################################
ARGVERTICES = -1
ARGEDGES = -1
def generateGraph(vertices):
    '''
        Returns a weighted directed graph with the number of edges and vertices specificed
    Params: vertices - integer of number of desired vertices, edges - integer of number of desired edges 
    '''
    graph = pygraph.graph.Graph(tag="weighted-directed")

    for vertex in range(0,vertices):
        # we add a colour for the benefit of the BFS used to check connectivity
        graph.add_vertex(vertex, pred = None, spEstimate= math.inf, colour = "white")

    # now we make edges to every other vertex in the graph
    # we will delete these later
    # for now we want to test passing arguments so I will just add one edge.
    for vertex in graph.vertices:
        for _vertex in graph.vertices:

            #continue so we have no self loops
            if vertex == _vertex:
                continue
            
            #random edge weight between 0-10
            weight = random.randint(0,10)
            graph.add_edge(vertex, _vertex, weight=weight)      
    return graph


def argumentHandeler():
    '''
        Function checks that the args passed to script are valid integers
        Params: 
        Returns: vertices - integer for vertices passed as arg1, edges - integer of edges passed as arg2
    '''
    #check argument passed for vertices is a valid Integer
    try:
        vertices = int(sys.argv[1])
    except:
        print("Argument passed for vertices was not an integer\n")
        print("Pass a valid integer")
        exit(1)

    try:
        edges = int(sys.argv[2])
    except:
        print("Argument passed for edges was not an integer\n")
        print("Pass a valid integer")
        exit(1)

    if vertices < 0 or edges < 0:
        print("Please input a positve integer for the vertices or edges")
        exit(1)    
    return vertices, edges             

def initialiseSingleSource(source, graph):
    '''
        Initialies input graph so vertex attributes are ready for BFS by setting non sourcce vertices colour to white, pred = none and 
        estimate to infinty, the source vertex is then set
        Params: Source -source vertex to start search from, graph, input graph been set
        Returns - graph - newly initialised graph
    '''
    for vertexIndex in graph.vertices:
        vertex = graph.vertices[vertexIndex]
        vertex['colour'] = "white"
        vertex['pred'] = None
        vertex['spEstimate'] = math.inf

    #Initialise source and make colour grey 
    sourceV = graph.vertices[source]
    sourceV['pred'] = None
    sourceV['spEstimate'] = 0
    sourceV['colour'] = 'Grey'

    return graph

def breadthFirstSearch(source, graph):
    '''
        Breadth first search that checks each vertex, we use this to ensure that the graph is fully connected when we remove edges in reduce function
        Params: source - source vertex to start search, graph - graph search is being made upon
        Returns: count - counter of number of vertices the search has visited.
    '''
    graph = initialiseSingleSource(source, graph)
    count = 0
    #no max size on queue to allow for graphs of different sizes
    queueBFS = queue.Queue()

    queueBFS.put(source)
    while queueBFS.empty() != True:
        vertex = queueBFS.get()
        neighbourVertices = graph.neighbours(vertex)
        
        for nVertex in neighbourVertices:
            #for each neighbour we check if they have been visted and add their predecessor and add the unvisted neighbours to the queue
            neighbourVertex = graph.vertices[nVertex]
            if neighbourVertex['colour'] == "white":
                neighbourVertex['colour'] = "Grey"
                neighbourVertex['pred'] = vertex
                queueBFS.put(nVertex)
                
        graph.vertices[vertex]['colour'] = "Black"
        count += 1
    return count

def removeEdge(graph, edge):
    '''
        Attempts to remove edge from graph and performs BFS
        to check if graph is connected, if it fails once, we try BFS on vertexV
        since vertexV may have an incoming edge and not an outgoing one
        Params: graph - graph the edge is removed from, vertexU - u vertex edge is being removed from, vertexV - v vertex edge is being removed from
        Returns: graph - graph with newly removed edge, boolean - verify if edge was sucesfully removed or not

    '''
    vertexU, vertexV = edge.get_incident_vertices()
    edgeWeight = edge['weight']
    graph.remove_edge(vertexU, vertexV)

    for v in graph.vertices:
        countFromBFS = breadthFirstSearch(v, graph) 

        if countFromBFS == ARGVERTICES:
            return graph, True

    graph.add_edge(vertexU, vertexV, weight = edgeWeight)
    return graph, False    


def reduceGraph(graph, edges):
    '''
        Function starts removing edges, using BFS to ensure no edge removed is a cut edge,
        repreated until number of edges is equal to ARGEDGES.
        Params: graph - graph to remove edges from, edges - number of edges we want
    '''
    edgeCount = 0
    edgeList = []
    # #counts the number of edges in the graph
    for edge in graph.edges:
        edgeList.append(edge)

    edgeCount = len(edgeList)

    while edgeCount != edges:
        
        #pull random edge from list
        randomEdgeIndex = random.randint(0, len(edgeList)-1)
        edge = edgeList[randomEdgeIndex]
        
        #removeEdge function does BFS on all vertices to check graph is connected
        graph, edgeCheck = removeEdge(graph, edge)

        #if check fails then we try a different edge
        if edgeCheck == False:
            continue
        edgeList.remove(edge)    

        #we decrease our edge count by 1
        edgeCount -= 1
        print("Edge Count:", edgeCount)    
    return graph

def pickleGraph(graph):
    '''
        Function pickles graph by writing it to an object file
        Params: Graph - graph object we want to write to a binary file
        Returns:
    '''
    filename = "Graph-" + str(ARGVERTICES)+"-"+ str(ARGEDGES) + ".p"
    pickle.dump(graph, open(filename,"wb"))

#This code which generates, reduces and pickles the graph is commented out so that the unit tests work.
#Uncomment and run to generate your own graphs, but this will cause the repective unit test to fail!
#It fails beacuse we need to import the functions into test_shortestpath.py this code runs automatically when imported
#Since there are no command line arguments that are passed to it an error is thrown.

# ARGVERTICES, ARGEDGES =  argumentHandeler()
# graph = generateGraph(ARGVERTICES)
# graph = reduceGraph(graph,ARGEDGES)
# pickleGraph(graph)

