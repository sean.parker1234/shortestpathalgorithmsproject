import pytest
import ShortestPath as SP 
import random
import math
import numpy as np
import generateGraph as GP

@pytest.fixture
def undirectedGraph():
    ''' Returns an undirected graph'''
    return SP.CreateWeightedDirectedGraph()

@pytest.fixture
def directedGraph():
    ''' Returns a directed graph'''
    return SP.CreateWeightedDirectedGraph()

@pytest.fixture
def negativeDirectedGraph():
    ''' Returns a directed graph with a negative weight cycle '''
    return SP.CreateWeightedDirectedNegativeGraph()

@pytest.fixture
def negativeWeightCycle():
    '''Returns a graph with a negaative weight cycle'''
    return SP.CreateNegativeWeightCycleGraph()   

@pytest.fixture
def negativeDirectedGraphWeightMatrix():
    '''Returns a SP weight matrix for the directed graph'''
    return np.array([[0.0, 2.0, 4.0, 7.0, -2.0],
                    [-2.0, 0.0, 2.0, 5.0, -4.0],
                    [-4.0, -2.0, 0.0, 3.0, -6.0],
                    [-7.0, -5.0, -3.0, 0.0, -9.0],
                    [2.0, 4.0, 6.0, 9.0, 0.0]])

@pytest.fixture
def directedGraphWeightMatrix():
    '''Returns a SP weight matrix for the directed graph'''
    return np.array([[0.0, 3.0, 9.0, 5.0, 11.0],
                    [11.0, 0.0, 6.0, 2.0, 8.0],
                    [5.0, 8.0, 0.0, 10.0, 2.0],
                    [9.0, 1.0, 4.0, 0.0, 6.0],
                    [3.0, 6.0, 7.0, 8.0, 0.0]])

class TestBellman:
    def testWeightedDirectedGraphReturnsTrue(self, directedGraph):
        source = random.randint(0,len(directedGraph.vertices)-1)

        bellman = SP.BellmanFord(directedGraph)
        directedGraph,success = bellman.run_algorithm(source)
        assert success == True
        
    def testNegativeDirectedReturnsTrue(self, negativeDirectedGraph):
        source = random.randint(0,len(negativeDirectedGraph.vertices)-1)

        bellman = SP.BellmanFord(negativeDirectedGraph)
        negativeDirectedGraph, success = bellman.run_algorithm(source)
        assert success == True

    def testNegativeWeightCycleDetection(self, negativeWeightCycle):
        source = random.randint(0,len(negativeWeightCycle.vertices)-1)

        bellman = SP.BellmanFord(negativeWeightCycle)
        graph, success = bellman.run_algorithm(source)
        
        if source == 3:
            assert success == True
        else:
            assert success == False    

    def testAllCorrectShortestPathsWeights(self, directedGraph):
        #actual path weights for directed graph
        weights = [ [0,3,9,5,11],
                    [11,0,6,2,8],
                    [5,8,0,10,2],
                    [9,1,4,0,6],
                    [3,6,7,8,0],    
        ]
        for vertex in directedGraph.vertices:
            source = vertex
            bellman = SP.BellmanFord(directedGraph)
            graph,success = bellman.run_algorithm(source)
            weightsOfPathFromBellman = bellman.get_all_path_weights()
            #source vertex will be the correct index of weights matrix for that source 
            assert weights[source] == weightsOfPathFromBellman

    def testAllCorrectShortestPathWeightsNegativeGraph(self, negativeDirectedGraph):
        #actul path weights for negative directed graph
        weights = [  [0,2,4,7,-2],
                    [-2,0,2,5,-4],
                    [-4,-2,0,3,-6],
                    [-7,-5,-3,0,-9],
                    [2,4,6,9,0],    
        ]

        for vertex in negativeDirectedGraph.vertices:
            source = vertex
            bellman = SP.BellmanFord(negativeDirectedGraph)
            graph, success = bellman.run_algorithm(source)
            weightsOfPathFromBellman = bellman.get_all_path_weights()
            #source vertex will be the correct index of weights matrix for that source 
            assert  weights[source] == weightsOfPathFromBellman

class TestUtilityFunctions():
    def testRelaxation(self, directedGraph):
        '''Testing edge {0,1} and edge {3,4}, 1st should relax 2nd should not change any values'''
        
        #expected values after relax
        testCase = [[3,0], [math.inf, None]]
        directedGraph = SP.InitializeSingleSource(directedGraph, 0)

        resultCase = SP.Relax(1,0,directedGraph.edges[0][1], directedGraph)

        assert testCase[0][0] == resultCase[0] and testCase[0][1] == resultCase[1]

        resultCase = SP.Relax(4,3, directedGraph.edges[3][4], directedGraph)

        assert testCase[1][0] == resultCase[0] and testCase[1][1] == resultCase[1]

    def testInitilization(self, directedGraph):
        #random source node selected
        source = random.randint(0,len(directedGraph.vertices)-1)

        directedGraph = SP.InitializeSingleSource(directedGraph, source)

        for vertex in directedGraph.vertices:

            if vertex != source:
                assert directedGraph.vertices[vertex]['pred'] == None and directedGraph.vertices[vertex]['spEstimate'] == math.inf
            else:
                assert directedGraph.vertices[vertex]['pred'] == None and directedGraph.vertices[vertex]['spEstimate'] == 0


class TestDijkstra():
    def testWeightedDirectedReturnsTrue(self, directedGraph):
        source = random.randint(0,len(directedGraph.vertices)-1)

        dijkstra = SP.Dijkstra(directedGraph)

        directedGraph, success = dijkstra.run_algorithm(source)
        assert success == True

    def testNegativeDirectedReturnsFalse(self, negativeDirectedGraph):
        source = random.randint(0,len(negativeDirectedGraph.vertices)-1)

        dijkstra = SP.Dijkstra(negativeDirectedGraph)

        directedGraph, success = dijkstra.run_algorithm(source)
        assert success == False
        

    def testCorrectShortestPath(self, directedGraph):
        #actual weights for source = 0
        source = 0
        
        weights = [0,3,9,5,11]

        dijkstra = SP.Dijkstra(directedGraph)
        directedGraph, success = dijkstra.run_algorithm(source)

        dijkstraWeights = dijkstra.get_all_path_weights()
        assert weights == dijkstraWeights

class TestFloydWarshall():
    def testCorrectShortestPathDirectedGraph(self, directedGraph, directedGraphWeightMatrix):
        floydWarshall = SP.FloydWarshall(directedGraph)

        returnedWeightMatrix, predecessorMatrix, success = floydWarshall.run_algorithm()
        assert success == True
        assert np.array_equal(returnedWeightMatrix, directedGraphWeightMatrix) == True 

    def testCorrectShortestPathNegativeDirectedGraph(self, negativeDirectedGraph,negativeDirectedGraphWeightMatrix ):
        floydWarshall = SP.FloydWarshall(negativeDirectedGraph)

        returnedWeightMatrix, predecessorMatrix, success = floydWarshall.run_algorithm()
        assert success == True
        assert np.array_equal(returnedWeightMatrix, negativeDirectedGraphWeightMatrix) == True

    def testNegativeWeightCycleDetection(self, negativeWeightCycle):
        floydWarshall = SP.FloydWarshall(negativeWeightCycle)

        returnedWeightMatrix, predecessorMatrix, success = floydWarshall.run_algorithm()  
        assert success == False
class TestJohnsons():
    def testCorrectShortestPathDirectedGraph(self, directedGraph, directedGraphWeightMatrix):
        johnsons = SP.Johnsons(directedGraph)
        returnedWeightMatrix, predecessorMatrix, success = johnsons.run_algorithm()
        assert success == True
        assert np.array_equal(returnedWeightMatrix, directedGraphWeightMatrix) == True 

    def testCorrectShortestPathNegativeDirectedGraph(self, negativeDirectedGraph,negativeDirectedGraphWeightMatrix ):
        floydWarshall = SP.FloydWarshall(negativeDirectedGraph)
        johnsons = SP.Johnsons(negativeDirectedGraph)
        weightMatrixF, predecessorMatrixF, success = floydWarshall.run_algorithm()
        weightMatrixJ, predecessorMatrixJ, success = johnsons.run_algorithm()
        assert success == True
        assert np.array_equal(predecessorMatrixJ, predecessorMatrixF) == True

class TestGenerateGraph():
    def testBreadthFirstSearch(self, directedGraph):
        countFromBFS = GP.breadthFirstSearch(0,directedGraph)

        assert countFromBFS == 5

    def testGeneratedGraph(self):
        graph = GP.generateGraph(5)

        vertexCount = 0
        for vertex in graph.vertices:
            vertexCount += 1

        assert vertexCount == 5

    def testRemovingEdgeConnected(self, directedGraph):
        directedGraph.remove_edge(0,3)

        #do BFS from a vertex to show the graph is connected
        countFromBFS = GP.breadthFirstSearch(0, directedGraph)

        assert countFromBFS == 5

    def testRemvoingEdgeUnconnected(self, directedGraph):
        directedGraph.remove_edge(0,3)
        directedGraph.remove_edge(0,1)

        #do BFS from a vertex to show the graph is connected
        countFromBFS = GP.breadthFirstSearch(0, directedGraph)

        assert countFromBFS == 1


        














