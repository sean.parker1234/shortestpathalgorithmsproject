
class VertexView():

    def __init__(self, graph):
        self.__graph = graph

    def __getitem__(self, key):
        return VertexDataView(self.__graph, key)

    def __contains__(self, item):
        return item in self.__graph

    def __iter__(self):
        return iter(self.__graph._vertices.keys())

    def __len__(self):
        return self.__graph.order()

    def __repr__(self):
        return str([x for x in self.__graph._vertices.keys()])

    def __str__(self):
        return self.__repr__()

class VertexDataView():

    def __init__(self, graph, vertex):
        self.__graph = graph
        self.__vertex = vertex

    def __getitem__(self, key):
        return self.__graph._vertex_attr[self.__vertex][key]

    def __setitem__(self, key, value):
        self.__graph._vertex_attr[self.__vertex][key] = value

    def __repr__(self):
        return str(self.__graph._vertex_attr[self.__vertex])

    def __str__(self):
        return self.__repr__()

class EdgeView():

    def __init__(self, graph):
        self.__graph = graph

    def __getitem__(self, key):
        return NeighbourView(self.__graph, key)

    def __iter__(self):
        edges = []
        for v in self.__graph._vertices.keys():
            edges += [EdgeDataView(self.__graph, v, x) for x in self.__graph._vertices[v].keys()]
        return iter(edges)

    def __repr__(self):
        return str(list(self.__iter__()))

class NeighbourView():

    def __init__(self, graph, v):
        self.__graph = graph
        self.__v = v

    def __getitem__(self, u):
        return EdgeDataView(self.__graph, self.__v, u)

    def neighbours(self):
        return list(self.__graph._vertices[self.__v].keys())

    def __iter__(self):
        return iter([EdgeDataView(self.__graph, self.__v, x[0]) for x in self.__graph._vertices[self.__v].items()])

    def __repr__(self):
        return str(list(self.__iter__()))

    def __str__(self):
        return self.__repr__()

class EdgeDataView():

    def __init__(self, graph, u, v):
        self.__graph = graph
        self.__u = u
        self.__v = v

    def get_incident_vertices(self):
        return [self.__u, self.__v]

    def __getitem__(self, key):
        return self.__graph._vertices[self.__u][self.__v][key]

    def __setitem__(self, key, value):
        self.__graph._vertices[self.__u][self.__v][key] = value

    def __repr__(self):
        return "%s -> %s :%s" % (self.__u, self.__v, str(self.__graph._vertices[self.__u][self.__v]))

    def __str__(self):
        return self.__repr__()

class Graph():

    graph_attr_dict_factory = dict
    vertex_dict_factory = dict
    vertex_attr_dict_factory = dict
    adjlist_outer_dict_factory = dict

    edge_attr_dict_factory = dict


    def __init__(self, **attr):

        self.graph_attr_dict_factory = self.graph_attr_dict_factory
        self.vertex_dict_factory = self.vertex_dict_factory
        self.vertex_attr_dict_factory = self.vertex_attr_dict_factory
        self.adjlist_outer_dict_factory = self.adjlist_outer_dict_factory
        self.edge_attr_dict_factory = self.edge_attr_dict_factory

        self._graph_attr = self.graph_attr_dict_factory()
        self._vertices = self.vertex_dict_factory()
        self._vertex_attr = self.vertex_attr_dict_factory()

        self._graph_attr.update(attr)

    def add_vertex(self, vertex, **attr):
        self._vertices[vertex] = self.adjlist_outer_dict_factory()
        self._vertex_attr[vertex] = self.vertex_attr_dict_factory()

        self._vertex_attr[vertex].update(attr)

    def add_vertices(self, vs):
        for v in vs:
            self.add_vertex(s)

    def remove_vertex(self, vertex):
        for v in self._vertices.keys():
            try:
                del self._vertices[v][vertex]
            except:
                pass

        del self._vertex_attr[vertex]
        del self._vertices[vertex]

    def remove_vertices(self, vs):
        for v in vs:
            self.remove_vertex(v)

    def add_edge(self, v, u, **attr):
        self._vertices[v][u] = self.edge_attr_dict_factory()
        self._vertices[v][u].update(attr)

    def remove_edge(self, v, u):
        del self._vertices[v][u]

    def is_adjacent(self, v, u):
        return u in self.neighbours(v)

    def neighbours(self, vertex):
        return self._vertices[vertex].keys()

    def order(self):
        return len(self._vertices)

    def size(self):
        count = 0
        for v in self._vertices.keys():
            count += len(self._vertices[v])
        return count

    @property
    def vertices(self):
        return VertexView(self)

    @property
    def edges(self):
        return EdgeView(self)

    def adjacency_matrix(self, attr_name=None):
        ret = []

        for v in self._vertices.keys():
            row = []
            for u in self._vertices.keys():
                if self.is_adjacent(v,u):
                    if not attr_name:
                        row.append(1)
                    else:
                        row.append(self._vertices[v][u][attr_name])
                else:
                    row.append(0)

            ret.append(row)
        return ret

    def __contains__(self, item):
        return item in self._vertices

    def __len__(self):
        return self.order()

    def __getitem__(self, n):
        if isinstance(n, slice):
            raise Exception("Graph attributes do not support slicing")
        return self._graph_attr[n]

    def __setitem__(self, a, b):
        self._graph_attr[a]=b
