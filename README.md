 ShortestPathAlgorithmsProject

Individual Project SC17SP repository 
PLEASE READ ME
Installation:
The same information as seen here can be seen in the installation guide in the repository wiki.

THIS IS VERY IMPORTANT
The algorithms use the pygraph library provided by Sam Wilson, Leeds University.
The pygraph module is in the project repository and should not be removed, otherwise the shortest path algorithms will not work.

ANACONDA:
All required packages used in this project are within:

ANACONDA: 4.9.2

Python: 3.8.5

If ANACONDA is not an option the following installations are required:

Python: 3.8.5

Numpy: 1.19.2

Pandas: 1.1.3

Path: 15.0.0

Openpyxl: 3.0.7

To install the required libraries run the following command from within the directory containing the requirements.txt file.
```
pip install -r requirements.txt
```

All other required libraries are path of the python standard library, they are as follows:
Math

Sys

Random

Queue

Pickle

Os

Time

**Execution:**
There are two primary ways of executing the algorithms.

1) Import the ShortestPath.py file into an existing python script
2) Run the algorithms from the ShortestPath.py main file.

**Import**

To import the script into another python script, ensure the ShortestPath.py file is in the current directory of the python script in question.
Then use the following:

`import ShortestPath as SP`

Now any of the methods and classes can be used from the ShortestPath file.

**From Main File**

You can added code to the main part of the python file to run the algorithms.
to execute the python file use the following:

`python ShortestPath.py`
